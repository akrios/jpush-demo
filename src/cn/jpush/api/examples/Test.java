package cn.jpush.api.examples;

import cn.jpush.api.JPushClient;
import cn.jpush.api.push.PushResult;
import cn.jpush.api.push.model.Options;
import cn.jpush.api.push.model.Platform;
import cn.jpush.api.push.model.PushPayload;
import cn.jpush.api.push.model.audience.Audience;
import cn.jpush.api.push.model.notification.AndroidNotification;
import cn.jpush.api.push.model.notification.IosNotification;
import cn.jpush.api.push.model.notification.Notification;

public class Test {
//	private static final String appKey ="354ff93e5c4fc20e5eef4c39";
//	private static final String masterSecret = "5c10651fb7b664da5049f55e";
	private static final String appKey ="d2e5101ca3d630804dc22a5d";
	private static final String masterSecret = "7948117114a203d5e0dbba16";

	public static void main(String[] args) {
	 JPushClient jpushClient = new JPushClient(masterSecret, appKey);
	 
//	 String jsonStr = "{\"platform\":\"all\",\"audience\":\"all\",\"notification\":{\"alert\":\"all\"},\"options\":{\"sendno\":1170676339}}";
//	 String jsonStr = "{\"platform\":\"all\",\"audience\":{\"tag\":[\"JPush\"]},\"notification\":{\"alert\":\"你好，我是消息推送测试111abc\"}}";
//	 String jsonStr = "{\"platform\":\"all\",\"audience\":{\"tag\":[\"宁波市\"]},\"notification\":{\"alert\":\"宁波您有新的订单可以抢\", \"sound\":\"default\", \"badge\":\"1\"},\"options\":{\"sendno\":1955299743, \"apns_production\":\"false\"}}";
//	 String jsonStr = "{\"platform\":\"all\",\"audience\":{\"tag\":[\"浙江省\"]},\"notification\":{\"alert\":\"浙江省您有新的订单可以抢\"},\"options\":{\"sendno\":1955299743, \"apns_production\":\"false\"}}";
//	 String jsonStr = "{\"platform\":\"all\",\"audience\":{\"tag\":[\"宁波市\"]},\"notification\":{\"ios\":{\"alert\":\"宁波您有新的订单可以抢\", \"sound\":\"default\", \"badge\":\"2\"}},\"options\":{\"sendno\":1955299743, \"apns_production\":\"false\"}}";
	 
	 PushPayload payload = PushPayload.newBuilder()
             .setPlatform(Platform.all())
             .setAudience(Audience.alias("123"))
             .setNotification(Notification.newBuilder()
                     .addPlatformNotification(IosNotification.newBuilder()
                             .setAlert("您有新的订单可抢")
                             .setSound("default")
                             .setBadge(1)
                             .build())
                     .build())
             .setOptions(Options.newBuilder().setApnsProduction(false).build())
             .build();
	 
	 PushResult result = jpushClient.sendPush(payload);
	 System.out.println(result.toString());
	}
}
